
/* TODO: fetch csv from influxdb
// const uri = `http://35.197.145.223:8086/query?q=SELECT mean("usage_user") AS "mean_usage_user", mean("usage_system") AS "mean_usage_system" FROM "telegraf"."autogen"."cpu" WHERE time > now() - 15m AND time < now() AND "cpu"='cpu-total' GROUP BY time(2500ms) FILL(null)&format=csv`;

// fetch(uri, {
//     method: 'GET',
//     headers: {
//         'Accept': 'text/csv'
//     }
// }).then(res => res.text()).then(data => {
//     // here
// }).catch(err => {
//     console.error(err);
// })
*/

d3.csv('data/mock.csv',

    // parse data
    function (d) {

        // parse unix micro second timestamp to date object;
        const parsedTime = new Date(d.time / 1000000);
        d.time = parsedTime;

        return d;
    },

    // now use the data 
    function (data) {

        const margin = {
            top: 50,
            right: 50,
            bottom: 50,
            left: 50
        };

        const width = 680 - margin.left - margin.right;
        const height = 480 - margin.top - margin.bottom;

        const svgContainer = d3.select('body').append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

        var x = d3.scaleTime()
            .domain(d3.extent(data, function (d) { return d.time; }))
            .range([0, width]);
        svgContainer.append('g')
            .attr('transform', 'translate(0,' + height + ')')
            .call(d3.axisBottom(x));

        var y = d3.scaleLinear()
            .domain([0, d3.max(data, function (d) { return +d.mean_usage_system; })])
            .range([height, 0]);
        svgContainer.append('g')
            .call(d3.axisLeft(y));

        // Add the area
        svgContainer.append('path')
            .datum(data)
            .attr('fill', '#cce5df')
            .attr('stroke', '#69b3a2')
            .attr('stroke-width', 1.5)
            .attr('d', d3.area()
                .x(function (d) { return x(d.time) })
                .y0(y(0))
                .y1(function (d) { return y(d.mean_usage_system) })
            );
    }
);
